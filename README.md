# Minimal code for Casper blockchain

This is an example how to create **non-boilerplate**, minimal code for Casper blockchain.

## Steps

1. Clone this repository:

```sh
$ git clone https://gitlab.com/andrzej1_1/minimal-casper-code.git
$ cd ./minimal-casper-code/
```

2. Make sure you have `wasm32-unknown-unknown` target installed for the current toolchain:

```sh
$ rustup target add wasm32-unknown-unknown
```

3. Build WebAssembly binary:

```sh
$ cargo build --release
```

**Done!** Compiled code will be located at `./target/wasm32-unknown-unknown/release/minimal_code.wasm`.

## Binary size

In my case binary size is 915625 bytes, which is dangerously close to 1 MB limit of deployment size.
